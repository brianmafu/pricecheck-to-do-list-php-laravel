
var currently_selected_list_id = null;

 $('#id_create_list_btn').click(function(){

   		$.ajax({
        type: "POST",
        url: "/create-list/",
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
        	'list_title': $('#id_list_title').val(),
        	'list_description': $('#id_list_description').val()
        }
        ,
        success: function(response) {
        	var response_data = response;
   			$("#list-items-data-section").html('');
   			currently_selected_list_id = response_data['list_id'];
          	$('#id_display_message').text('List (' + response_data['title'] + ') has been successfully created');
           	$("#id_message").modal('show');
           	$(".selected-list-heading").html('List Name: ' + response_data['title']);
        },
        error: function() {
            $('#id_display_message').text('an error has occured while trying to created list. Please try again.');
           	$("#id_message").modal('show');
        }
    });
  });
$('#id_create_list_item_btn').click(function(){

   		$.ajax({
        type: "POST",
        url: "/create-to-do-list-item/",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
        	'list_item_title': $('#id_list_item_title').val(),
        	'list_item_description': $('#id_list_item_description').val(),
            'to_do_list_id': currently_selected_list_id
        }
        ,
        success: function(response) {
        	var response_data = response;
        	   currently_selected_list_id  = response_data['list_id']
        		var complete = response_data['complete'] == true ? 'Yes' : 'No';
                console.log(response);
            	$("#list-items-data-section").append('<tr id="' + response_data['id'] +  '" data-id="'+response_data['id'] + '"><td>' + response_data['title'] + '</td><td>' + response_data['description'] + '</td><td>' +response_data['created'] + '</td><td class="complete_text" id="id_item_complete_'+ response_data['id'] + '">' + complete +'</td><td>Complete: <input class="complete_checkbox" type="checkbox" id="id_mark_item_complete_' + response_data['id'] + '"/></input></td></tr>');
            	$("#id_item_delete_"+response_data['']).hide();
            	$('#id_item_delete_' + response_data['id']).click(function(){
            		if (field_value['complete'] == false) {
            				$("#id_item_delete_"+response_data['id']).hide();
            		}
        			$.ajax({
        			url: '/delete-to-do-list-item/' + response_data['id'] + '/',
        			type: 'get',
        			success: function(data){
        				 $('#' + response_data['id']).remove();
        				 $('#id_display_message').text('Item (' + response_data['title'] + ') has been removed');
       					 $("#id_message").modal('show');
        			},
        			error: function() {
            			$('#id_display_message').text('an error has occured while trying to delet list item. Please try again.');
           				$("#id_message").modal('show');
        			}

            		});
            	});
    
        		$('#id_mark_item_complete_' + response_data['id']).click(function(){
            				$.ajax({
        					url: '/update-to-do-list-item-status/' + response_data['id'] + '/',
        					type: 'get',
        					success: function(data){
        						var response_data = data;
        						
        						var complete = response_data['complete'] == true ? 'Yes' : 'No';
        						var complete_alert_text = response_data['complete'] == true ? 'Complete' : 'Incomplete'
        						if(response_data['complete'] == true) {
        							$("#id_item_delete_"+response_data['id']).show();
        						} else {
        							$("#id_item_delete_"+response_data['id']).hide();
        						}
        						$("#id_item_complete_"+ response_data['id']).text(complete);
        				 		$('#id_display_message').text('Item (' + response_data['title'] + ') has been marked as ' + complete_alert_text);
       					 		$("#id_message").modal('show');
        					},
        					error: function() {
            					$('#id_display_message').text('an error has occured while trying to updating list item. Please try again.');
           						$("#id_message").modal('show');
        				}

            				});
            			});

          	$('#id_display_message').text('List Item (' + response_data['title'] + ') has been successfully created');
           	$("#id_message").modal('show');

        },
        error: function() {
            $('#id_display_message').text('an error has occured while trying to created list. Please try again.');
           	$("#id_message").modal('show');
        }
    });
});

$("#id_mark_all_list_items_complete").click(function(){
		$.ajax({
        type: "get",
        url: "/mark-all-list-items-as-complete/" + currently_selected_list_id + '/',
        success: function(data) {
        	$(".complete_text").text("Yes");
        	$(".complete_checkbox").attr("checked", "checked");
            $(".delete-link").show();
       		$('#id_display_message').text('All Items Marked As Completed');
           	$("#id_message").modal('show');
        },
        error: function() {
            $('#id_display_message').text('an error has occured while trying to updating list item. Please try again.');
           	$("#id_message").modal('show');
        }

});
});

 $('#id_all_lists').click(function(){

   		$.ajax({
        type: "get",
        url: "/all-to-do-lists/",
        success: function(response) {

            var response_data = response[0];
            var list_data = [];
            var count = 0;
            $.each(response_data, function(list_index, list_field_values){
            	count = count + 1;
                ///console.log(list_field_values);
            	list_data.push('<li class="list-group-item list-group-item-primary"><a href="#" id="id_link_list_item_'+ list_field_values['id'] +'">' + count + '. ' + list_field_values['title'] + '</a></li>');
            });
            if (list_data.length > 0 ) {
            		$('#all-list-groups').html(list_data.join(''));
            } else {
            	$('#all-list-groups').html('<li class="list-group-item list-group-item-primary">currently no lists available</list>');
            }
            // Register list item onclick callbacks
            $.each(response_data, function(list_index, list_field_values){
            	  $('#id_link_list_item_'+ list_field_values['id']).click(function(){
            	  	currently_selected_list_id = list_field_values['id']
            		$.ajax({
            			type: 'get',
            			url: '/get-to-do-list-items/' + currently_selected_list_id + '/',
            			success: function(data) {
                            $('#all_lists').modal('toggle'); 
            				var item_rows = []
            				var to_data_list_items_data = data[0];
            				$(".selected-list-heading").html('List Name: ' + list_field_values['title']);
            				$.each(to_data_list_items_data, function(field_key, field_value){
            				var complete = field_value['complete'] == true ? 'Yes' : 'No';
            				item_rows.push('<tr id="' + field_value['id'] +  '" data-id="'+field_value['id'] + '"><td>' + field_value['title'] + '</td><td>' + field_value['description'] + '</td><td>' +field_value['created_at'] + '</td><td class="complete_text" id="id_item_complete_'+ field_value['id'] + '">'+ complete +'</td><td><a class="delete-link" href="#" id="id_item_delete_' +  field_value['id'] + '"> Check Off</a><br>Complete: <input type="checkbox" class="complete_checkbox" id="id_mark_item_complete_' + field_value['id'] + '"/></td></tr>');

           
            			});
            			$("#list-items-data-section").html(item_rows.join(''));
            			$.each(to_data_list_items_data, function(field_key, field_value){ 
            			if (field_value['complete'] == false) {
            				$("#id_item_delete_"+field_value['id']).hide();

            			} else {
            				$("#id_mark_item_complete_" + field_value['id']).attr("checked", "checked")
                            $("#id_item_delete_"+field_value['id']).show();
            			}
            			$('#id_item_delete_' + field_value['id']).click(function(){
            			
        					$.ajax({
        					url: '/delete-to-do-list-item/' + field_value['id'] + '/',
        					type: 'get',
        					success: function(data){
        				 		$('#' + field_value['id']).remove();
        				 		$('#id_display_message').text('Item (' + field_value['title'] + ') has been removed');
       					 		$("#id_message").modal('show');
        					},
        					error: function() {
            					$('#id_display_message').text('an error has occured while trying to delet list item. Please try again.');
           						$("#id_message").modal('show');
        				}

            				});
        				});
        				$('#id_mark_item_complete_' + field_value['id']).click(function(){
   
            				$.ajax({
        					url: '/update-to-do-list-item-status/' + field_value['id'] + '/',
        					type: 'get',
        					success: function(data){
        						var response_data = data;
        						
        						var complete = response_data['complete'] == true ? 'Yes' : 'No';
        						if(response_data['complete'] == true) {
        								$("#id_item_delete_"+field_value['id']).show();
        						} else {
        							$("#id_item_delete_"+field_value['id']).hide();
        						}
        						var complete_alert_text = response_data['complete'] == true ? 'Complete' : 'Incomplete'
        						$("#id_item_complete_"+ response_data['id']).text(complete);
        				 		$('#id_display_message').text('Item (' + field_value['title'] + ') has been marked as ' + complete_alert_text);
       					 		$("#id_message").modal('show');
        					},
        					error: function() {
            					$('#id_display_message').text('an error has occured while trying to updating list item. Please try again.');
           						$("#id_message").modal('show');
        				}

            				});
            			});
            			});

            			}
            		});
            	});
            });
        },
        error: function() {
            $('#id_display_message').text('could not retrieve all to do lists.');
           	$("#id_message").modal('show');
        }
    });
  });

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class HomePageController extends Controller
{
	/**
     * Display a listing of tasks with form for adding new task.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	
    	return view('home');
    }


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\ToDoListItem;

class ToDoListItemsController extends Controller
{
	/**
     * gets all the to lists to_do_list.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_to_list_items()
    {
    	$to_do_list_items = ToDoListItem::orderBy('created_at', 'asc')->paginate(5);
    	
    	return Response::json(array('data' => $to_do_list_items));;
    }




    /**
     * saves a newley created to do list item into database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create_to_do_list_item(Request $request)
    {
  
        $to_do_list_item = new ToDoListItem;
        $to_do_list_item->title = $request->list_item_title;
        $to_do_list_item->description = $request->list_item_description;
        $to_do_list_item->to_do_list_id = $request->to_do_list_id;
        $to_do_list_item->save();
        return response()->json(array(
            'title'=> $to_do_list_item->title, 
            'list_id'=> $to_do_list_item->to_do_list_id,
            'id' => $to_do_list_item->id,
            'description' => $to_do_list_item->description,
            'complete' => False,
            'created' => $to_do_list_item->created_at->toDateTimeString()
        ));
    }

    /**
     * Remove the specified To Do List Item from the database.
     *
     * @param  \App\Task  $to_do_list_item
     * @return \Illuminate\Http\Response
     */
    public function delete_to_do_list_item(Request $request, $item_id)
    {
        $status = 'Success';
        try {
            $to_do_list_item = ToDoListItem::where('id', (int) $item_id)->first();
            $to_do_list_item->delete();
        } 
        catch(Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            $status = 'Failed';
        }

    	return response()->json(array(
            'status' => $status
        ));
    }

    /**
     * Update Status of the to do list item
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ToDoList  $to_do_list_item
     * @return \Illuminate\Http\Response
     */
    public function update_to_do_list_item_status(Request $request, $item_id)
    {
        $to_do_list_item = ToDoListItem::where('id', (int) $item_id)->first();
        $to_do_list_item->complete = !$to_do_list_item->complete;
        $to_do_list_item->save();

        return response()->json(array(
            "id" => $to_do_list_item->id,
            "complete" => $to_do_list_item->complete
        ));
    }

    #mark_all_to_list_items_as_complete Marks all lists items to complete
    public function mark_all_to_list_items_as_complete(Request $request, $list_id)
    {
        $to_do_list_items = ToDoListItem::where('to_do_list_id', (int) $list_id);
        $to_do_list_items->update(['complete' => True]);

        return response()->json(array(
            "id" => $list_id,
            "complete" =>True
        ));
    }
}

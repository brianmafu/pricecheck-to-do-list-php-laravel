<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\ToDoList;
use App\ToDoListItem;

class ToDoListController extends Controller
{
	/**
     * Display a lists of to_do_lists with form to add new to_do_lists.
     *
     * @return \Illuminate\Http\Response
     */
    public function all_to_do_lists()
    {
    	$to_do_lists = ToDoList::All();
    	
    	return response()->json(array($to_do_lists));
    }


    /**
     * Display a lists of to_do_lists with form to add new to_do_lists.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_to_do_list_items(Request $request, $last_id)
    {
        $to_do_list_id = (int) $last_id;
        $to_do_list_items = ToDoListItem::where('to_do_list_id', $to_do_list_id)->get();
        return response()->json(array($to_do_list_items));
    }
    /**
     * creates a new to do list.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create_to_do_list(Request $request)
    {
  
		$to_do_list = new ToDoList;
		$to_do_list->title = $request->list_title;
        $to_do_list->description = $request->list_description;
		$to_do_list->save();
        return response()->json(array('title'=> $to_do_list->title, 'list_id'=> $to_do_list->id));
    }

    /**
     * Remove the specified To Do List from the database.
     *
     * @param  \App\Task  $to_do_list
     * @return \Illuminate\Http\Response
     */
    public function delete_to_do_list(Request $request, $list_id)
    {
       $status = 'Success';
        try {
            $to_do_list = ToDoListItem::where('id', (int) $item_id)->first();
            $to_do_list->delete();
        } 
        catch(Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            $status = 'Failed';
        }

        return response()->json(array(
            'status' => $status
        ));
    }

    /**
     * Update Status of the to do list 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ToDoList  $to_do_list
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $to_do_list)
    {
        $to_do_list->complete = !$to_do_list->complete;
        $to_do_list->save();

        return redirect('/');
    }
}

    <!-- Add To List  Dialog -->
    <div id="add_to_list" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header login-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Add To List</h4>
                </div>
                <div class="modal-body">
                            <input type="text" placeholder="List Item Title" name="list-item-name" id='id_list_item_title'>
                            <textarea placeholder="Description" id='id_list_item_description'></textarea>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="cancel" data-dismiss="modal">Close</button>
                    <button type="button" class="add-to-list" id='id_create_list_item_btn' data-dismiss="modal">Add To List</button>
                </div>
            </div>

        </div>
    </div>
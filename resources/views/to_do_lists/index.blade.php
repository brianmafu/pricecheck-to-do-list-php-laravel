	<!-- Display All List Dialog -->
	<div id="all_lists" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header login-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Lists</h4>
                 <h5 class="modal-title">Click on List Item to View</h4>

            </div>
            <div class="modal-body">
				<ul class="list-group borderless" id='all-list-groups'>
  
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
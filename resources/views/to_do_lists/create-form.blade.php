  <!-- Create New List Dialog -->
   <div id="create_list" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header login-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Create List</h4>
            </div>
            <div class="modal-body">
                        <input type="text" placeholder="List Title" name="list-title" id='id_list_title'>
                        <textarea placeholder="Description" name='list-description' id='id_list_description'></textarea>
                </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Close</button>
                <button type="button" id='id_create_list_btn' class="create-list" data-dismiss="modal">Create</button>
            </div>
        </div>

    </div>
	</div>
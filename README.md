# Price Check To Do List App
This Project is a basic to do list app for creating lists and specifc items that must be completed by the user.
## Quick Installation
First, clone repo and install all dependencies.
```sh
$ https://gitlab.com/brianmafu/pricecheck-to-do-list-php-laravel.git
$ cd pricecheck-to-do-list-php-laravel
$ Please ensure that ```php artisan``` is working. Your may refer to : `[text](https://laravel.com/docs/5.7/installation`)
$ Install Composer
$ Run command ```composer install```
$ NB. Project uses uses the Laravel 5.5 Framework, please ensure that all laravel setup steps are met First
```

## Database Setup
After that, setting up database config in `.env` file and then run migrate command.
```sh 
$ Install Mysql Server. 
$ Create a database called: 'forge' with username set as 'root' and 'password' not set respectively
   NB. You have an option to  update  the .env file  in the main project folder with the credentials you prefer to use for your database.
$ Run the following.
$ php artisan migrate
```


## Running Project
Running the project is pretty simple. Run the following command and project will run on the following url by default: http://127.0.0.1:8000
```sh 
$ php artisan serve
```
You're ready to go! :)


<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Show task dashboard.
 */
Route::get('/', 'HomePageController@index');

/**
 * Add new To Do List.
 */
Route::post('/create-list/', 'ToDoListController@create_to_do_list');


/** 
*Add new List Item to To Do list
*/

Route::post('/create-to-do-list-item/', 'ToDoListItemsController@create_to_do_list_item');

# get all to lists as json string
Route::get('/all-to-do-lists/', 'ToDoListController@all_to_do_lists');


# returns a specific to_do_list items
Route::get('/get-to-do-list-items/{list_id}', 'ToDoListController@get_to_do_list_items');

#  change status of to do list item item to complete or to not complete

Route::get('/update-to-do-list-item-status/{item_id}', 'ToDoListItemsController@update_to_do_list_item_status');

#  change status of all status as comple

Route::get('/mark-all-list-items-as-complete/{list_id}', 'ToDoListItemsController@mark_all_to_list_items_as_complete');

# removes a to do list from the database

Route::get('/delete-to-do-list/{list_id}', 'ToDoListController@delete_to_do_list');

# removes to list item for a specific to do list

Route::get('/delete-to-do-list-item/{item_id}', 'ToDoListItemsController@delete_to_do_list_item');	